package ru.sbt.project.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import ru.sbt.project.service.ClassA;
import ru.sbt.project.service.UserPerformedService;

import java.time.Duration;

@Slf4j
@Configuration
@EnableConfigurationProperties(ru.sbt.project.config.ConfigurationRestGitProperties.class)
@ComponentScan("ru.sbt.project")
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(5))
                .setReadTimeout(Duration.ofSeconds(5))
                .build();
    }

    @Bean
    public ClassA invokeMessage(ClassA classA) {
        log.info("Configuration execute");
        classA.getMessageFromClassA();
        return classA;
    }

    @Bean
    public UserPerformedService invokeMessageFromClassC(UserPerformedService classC) {
        classC.getMessageFromClassC();
        return classC;
    }
}
