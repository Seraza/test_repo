package ru.sbt.project.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.sbt.project.service.ClassB;
import ru.sbt.project.service.TestClass;

@Slf4j
@Configuration
@ComponentScan("ru.sbt.project")
public class ControllerConfig {

    @Bean
    public ClassB someLogicForUsers(ClassB classB) {
        classB.addTemplateUser();
        classB.addUserWithLogin("Sample");
        classB.addUserWithLogin("Sample2");
        classB.createEmployee();
        log.info("Add users before start");
        return classB;
    }

    @Bean
    public TestClass testWriteLog(TestClass testClass) {
        testClass.testWriteLog();
        testClass.writeSomeParametresFromProperties();
        return testClass;
    }

}
