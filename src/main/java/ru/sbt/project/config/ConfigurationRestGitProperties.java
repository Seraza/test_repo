package ru.sbt.project.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Getter
@Setter
@ConfigurationProperties("mt.prop")
public class ConfigurationRestGitProperties {

    @NestedConfigurationProperty
    Template template = new Template();

    private String name;
    private String lastName;
    private String middleName;

    @Getter
    @Setter
    public static class Template {
        private String value_1;
        private String value_2;
    }
}
