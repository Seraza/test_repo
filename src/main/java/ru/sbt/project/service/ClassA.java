package ru.sbt.project.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sbt.project.config.ConfigurationRestGitProperties;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClassA {
    private final ClassB classB;
    private final ConfigurationRestGitProperties configurationRestGitProperties;

    public void getMessageFromClassA() {
        log.info("Execute method from class A");
        log.info("Name from config: {}, lastname: {}",
                configurationRestGitProperties.getName(), configurationRestGitProperties.getLastName());
        classB.message();
    }
}
