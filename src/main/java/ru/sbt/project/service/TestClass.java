package ru.sbt.project.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sbt.project.config.ConfigurationRestGitProperties;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestClass {
    private final ConfigurationRestGitProperties configurationRestGitProperties;

    public void testWriteLog() {
        log.info("THIS IS LOG!!!!");
    }

    public void writeSomeParametresFromProperties() {
        log.info(String.format("Param from prop: %s", configurationRestGitProperties.getName()));
        log.info("From class:");
        log.info(String.format("Param from app properties: %s",configurationRestGitProperties.getTemplate().getValue_1()));
        log.info(String.format("Param from app properties: %s",configurationRestGitProperties.getTemplate().getValue_2()));

    }
}
