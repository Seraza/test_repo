package ru.sbt.project.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sbt.project.dto.response.UserClass;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserPerformedService {

    public void getMessageFromClassC() {
        log.info("Execute class C");
    }

    public UserClass getUserByLogin(String login) {
        List<UserClass> userClassList = getListofUsers();
        return userClassList.stream().filter(el -> el.getLogin().equalsIgnoreCase(login)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("User is not exist"));
    }

    public List<UserClass> getAllUsers() {
        return getListofUsers();
    }

    public String getLoginByIdFromUsers(String id) {
        List<UserClass> userClassList = getListofUsers();
        return userClassList.stream().filter(el -> el.getId().equalsIgnoreCase(id)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("User is not exist")).getLogin();
    }

    private List<UserClass> getListofUsers() {
        List<UserClass> userClassList = new ArrayList<>();
        userClassList.add(new UserClass("firstUser","1"));
        userClassList.add(new UserClass("secondUser","2"));
        userClassList.add(new UserClass("Asa3","3"));
        userClassList.add(new UserClass("Nbx4","4"));
        userClassList.add(new UserClass("Yty5","5"));
        return userClassList;
    }
}
