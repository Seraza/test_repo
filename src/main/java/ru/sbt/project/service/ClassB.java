package ru.sbt.project.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sbt.project.controllers.TestUsersController;
import ru.sbt.project.dto.response.UserClass;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClassB {

    public void message() {
        log.info("Work class B");
    }

    private  final TestUsersController testUsersController;

    public void addTemplateUser(){
        testUsersController.addUserSample();
    }

    public void addUserWithLogin(String login){
        testUsersController.addUserWithLogin(login);
    }

    public void createEmployee()
    {
      //  final String uri = "http://localhost:8080/userss/createUser";
      //  RestTemplate restTemplate = new RestTemplate();

        UserClass newEmployee = new UserClass("Adam", "7");

     /*   UserClass result = restTemplate.postForObject( uri, newEmployee, UserClass.class);
*/
      //  System.out.println(result);
       // testUsersController.createUser(newEmployee);
    }
}
