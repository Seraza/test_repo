package ru.sbt.project.service;

import java.util.List;
import ru.sbt.project.dto.response.UserClass;


public interface SiteService {

    List<UserClass> findAllUsers();
    String findUserByLogin(String login);
}
