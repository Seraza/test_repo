package ru.sbt.project.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("json")
@RequiredArgsConstructor
public class JsonController {

    ObjectMapper objectMapper = new ObjectMapper();
    Map<String, Object> map = new HashMap<>();

    @GetMapping(value = "/getMap", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getMapFromJsonString() {
        String jdonString = "{\n" +
                "  \"compilerOptions\": {\n" +
                "    \"target\": \"es5\",\n" +
                "    \"lib\": [\n" +
                "      \"es6\",\n" +
                "      \"dom\",\n" +
                "      \"dom.iterable\",\n" +
                "      \"esnext\"\n" +
                "    ],\n" +
                "    \"allowJs\": true,\n" +
                "    \"noImplicitReturns\": true,\n" +
                "    \"jsx\": \"react-jsx\"\n" +
                "  },\n" +
                "  \"include\": [\n" +
                "    \"src\"\n" +
                "  ], \"ddd\": 123 \n" +
                "}";
        try {
            map = (Map<String, Object>) objectMapper.readValue(jdonString, Object.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        for (Map.Entry nextEntry : map.entrySet()) {
            System.out.println("> Key: " + nextEntry.getKey() + ". Value: "
                    + nextEntry.getValue());
        }
        return map;
    }

    @GetMapping(value = "/getJson", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getJsonFromMap() {
        try {
           return objectMapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

}
