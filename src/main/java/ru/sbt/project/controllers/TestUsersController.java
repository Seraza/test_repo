package ru.sbt.project.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.sbt.project.dto.response.UserClass;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("userss")
@RequiredArgsConstructor
public class TestUsersController {

    private final List<UserClass> users = new ArrayList<>();

    @GetMapping(value = "/findAllUsers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserClass> findAllUsers() {
        return users;
    }

    @GetMapping(value = "/addUserTemplate", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserClass> addUserSample() {
        users.add(new UserClass("Template ","0"));
        return users;
    }

    @GetMapping(value = "/addUserFromLogin/{login}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserClass> addUserWithLogin(@PathVariable String login) {
        users.add(new UserClass(login,String.valueOf(users.size())));
        return users;
    }

    @PostMapping(value = "/createUser")
    public ResponseEntity<String> createUser (UserClass userClass) {
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{login}")
                .buildAndExpand(userClass.getLogin())
                .toUri();
        return ResponseEntity.created(location).build();
    }

}
