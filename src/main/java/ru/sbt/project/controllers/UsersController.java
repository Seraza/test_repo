package ru.sbt.project.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sbt.project.dto.response.UserClass;
import ru.sbt.project.service.UserPerformedService;


import java.util.List;

@Slf4j
@RestController
@RequestMapping("user")
@RequiredArgsConstructor
public class UsersController {

    private final UserPerformedService userPerformedService;

    @GetMapping(value = "/findAllUsers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserClass> findAllUsers() {
        return userPerformedService.getAllUsers();
    }

    @GetMapping(value = "/findUserByLogin/{login}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserClass findUserByLogin(@PathVariable String login) {
      return userPerformedService.getUserByLogin(login);
    }

    @GetMapping(value = "/findUserLoginById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getLoginFromUserById(@PathVariable String id) {
        return userPerformedService.getLoginByIdFromUsers(id);
    }
}
