package ru.sbt.project.dto.response;

import lombok.*;

@ToString
@Data
@AllArgsConstructor
public class UserClass {
    private String login;
    private String id;
}
