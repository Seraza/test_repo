package ru.sbt.project;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;
import ru.sbt.project.dto.response.UserClass;
import ru.sbt.project.service.GitHubRest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes=
//@SpringBootTest
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GitHubTest {
    private static final String baseUrlGitHub = "https://api.github.com";

    @Autowired
    public GitHubRest gitHubRest;
   // private TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Test
     void testFindAllMethod() {
        //  siteService = new GitHubRest(new RestTemplate());
     //   List<UserClass> res = gitHubRest.findAllUsers(baseUrlGitHub, "/users");
      //  res.forEach(i ->
      //          System.out.println(i.toString()));
    }

  //  @Test
   /*  void testFindOneMethod() {
        testRestTemplate.
        ResponseEntity<UserClass> responceEntity = restTemplate.exchange("http://localhost:8080/user/findAllUsers", HttpMethod.GET, null, UserClass.class);
         System.out.println(responceEntity);
         UserClass responce= responceEntity.getBody();
        System.out.println(responce);
    }*/

    @Test
    void getMigrationPackByNameTestIntegration() {
        ResponseEntity<String> response = restTemplate
                .getForEntity("http://localhost:" + port + "/user/findAllUsers", String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void getMigrationPackByNameTest() throws Exception {
        String login = "firstUser";
        mockMvc.perform(MockMvcRequestBuilders.get(
                "/user/findUserByLogin/{login}", login)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        //.andExpect(MockMvcResultMatchers.jsonPath("packName").exists());
    }

    @Test
    public void createEmployee()
    {
          final String uri = "http://localhost:8080/userss/createUser";
          RestTemplate restTemplate = new RestTemplate();

        UserClass newEmployee = new UserClass("Adam", "7");

           UserClass result = restTemplate.postForObject( uri, newEmployee, UserClass.class);

          System.out.println("AAA" + result);
        // testUsersController.createUser(newEmployee);
    }

}
